/*
 * Copyright (c) 2018 Aegaeon IT S.A.. All Rights Reserved
 * This program is a trade secret of Aegaeon IT S.A., and it is not to be
 * reproduced, published, disclosed to others, copied, adapted, distributed
 * or displayed without the prior authorization of Aegaeon IT S.A..
 * Licensee agrees to attach or embed this notice on all copies of the
 * program, including partial copies or modified versions thereof, and
 * is licensed subject to restrictions on use and distribution.
 */
package simplerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
	

/**
 * @author brenuart
 *
 */
@SpringBootApplication
@EnableConfigurationProperties(SimpleRestProperties.class)
public class SimpleRestApplication {

	public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder(SimpleRestApplication.class).web(true).build();
        app.run(args);
	}

}
