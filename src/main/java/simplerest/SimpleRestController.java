/*
 * Copyright (c) 2018 Aegaeon IT S.A.. All Rights Reserved
 * This program is a trade secret of Aegaeon IT S.A., and it is not to be
 * reproduced, published, disclosed to others, copied, adapted, distributed
 * or displayed without the prior authorization of Aegaeon IT S.A..
 * Licensee agrees to attach or embed this notice on all copies of the
 * program, including partial copies or modified versions thereof, and
 * is licensed subject to restrictions on use and distribution.
 */
package simplerest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author brenuart
 *
 */
@RestController
@RequestMapping(value="/api/v1", produces=MediaType.APPLICATION_JSON_VALUE)
public class SimpleRestController {

	@RequestMapping(value = "/hello", method = { GET, HEAD })
	public String hello() {
		return "Hello world!";
	}

/*
	public String stupidMethod() {
		String str = null;
		return str.toString();
	}
*/
}
