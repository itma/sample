package contracts

org.springframework.cloud.contract.spec.Contract.make {
	request {
		method 'GET'
		url '/api/v1/project/unknown'
		headers {
			contentType('application/json')
		}
	}
	response {
		status 404
		body([
			"status": 404,
			"path": "/api/v1/project/unknown"
		])
		headers {
			contentType('application/json')
		}
	}
}


/*
{
  "timestamp": "2018-10-10T17:21:53.523+02:00",
  "status": 404,
  "error": "projectmeta.exception.ResourceNotFoundException",
  "message": "Resource not found with id='a'",
  "path": "/api/v1/project/a",
  "traceId": "a16650053bb33454",
  "spanId": "a16650053bb33454"
}
*/