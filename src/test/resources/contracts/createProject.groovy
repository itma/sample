package contracts

org.springframework.cloud.contract.spec.Contract.make {
	request {
		method 'POST'
		url '/api/v1/project'
		body([
			"id": "sample",
			"name": "Sample Project",
			"repoUrl": "http://localhost"
		])
		headers {
			contentType('application/json')
		}
	}
	response {
		status 201
		body([
			"id": "sample",
			"name": "Sample Project",
			"repoUrl": "http://localhost"
		])
		headers {
			contentType('application/json')
		}
	}
}