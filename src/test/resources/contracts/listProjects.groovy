package contracts

org.springframework.cloud.contract.spec.Contract.make {
	request {
		method 'GET'
		url '/api/v1/project'
		headers {
			accept('application/json')
		}
	}
	response {
		status 200
		body([ []
		])
		headers {
			contentType('application/json')
		}
	}
}
