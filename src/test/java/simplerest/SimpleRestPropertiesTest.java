/*
 * Copyright (c) 2019 Aegaeon IT S.A.. All Rights Reserved
 * This program is a trade secret of Aegaeon IT S.A., and it is not to be
 * reproduced, published, disclosed to others, copied, adapted, distributed
 * or displayed without the prior authorization of Aegaeon IT S.A..
 * Licensee agrees to attach or embed this notice on all copies of the
 * program, including partial copies or modified versions thereof, and
 * is licensed subject to restrictions on use and distribution.
 */
package simplerest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

/**
 * @author brenuart
 *
 */
public class SimpleRestPropertiesTest {

	@Test
	public void doit() {
		SimpleRestProperties props = new SimpleRestProperties();
		assertThat(props.getaString()).isNull();
	}
}
